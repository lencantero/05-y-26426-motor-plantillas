const express = require("express");
const router = express.Router();
const { getALL} = require ("../db/conexion");
const {db} = require("../db/conexion");
const { EMPTY } = require("sqlite3");



const fs = require("fs");
const multer = require("multer");
const upload = multer({ dest: "./public/assets/images/"});
const fileUpload = upload.single("url");

router.get("/", (req, res)=>{
    res.render("admin/index");
});

router.get('/integrantes/listar', (req, res) =>{
    const sql = "SELECT * FROM Integrantes";
    db.all(sql, (err, integrantes) => {
        if (err) {
            console.error(err.message);
            res.status(500).send("Error interno del servidor");
            return;
        }
        res.render("admin/integrantes/index", {
            integrantes: integrantes,
        });
    });
});



router.get("/TipoMedia/listar", async (req, res)=>{
    const tipo_media = await getALL("SELECT * FROM TipoMedia");
    console.log(tipo_media)
    res.render("admin/TipoMedia/index", 
    {
            tipo_media: tipo_media
    }
    );
});
router.get("/Media/listar", async (req, res)=>{
    const media = await getALL("SELECT * FROM Media");
    console.log(media)
    res.render("admin/Media/index", 
    {
            media: media
    }
    );
});

router.get("/integrantes/crear",(req, res)=>{
    const mensaje = req.query.mensaje;
    res.render("admin/integrantes/crearForm",{
    mensaje : mensaje
    });
});
router.get("/TipoMedia/crear",(req, res)=>{
    const mensaje = req.query.mensaje;
    res.render("admin/TipoMedia/crearForm",{
    mensaje:mensaje

    });
});

router.get("/Media/crear",(req, res)=>{
    const mensaje = req.query.mensaje;
    res.render("admin/Media/crearForm",{
    mensaje:mensaje

    });
});



//obtener datos del formulario
router.post('/integrantes/create', fileUpload, (req, res) =>{
    //validaciones
    if (!req.body.matricula || req.body.matricula.trim() === ''
        || !req.body.nombre || req.body.nombre.trim() === ''
        || !req.body.apellido || req.body.apellido.trim() === ''
        || !req.body.url || req.body.url.trim() === ''
        || !req.body.activo || req.body.activo.trim() === '')
     return res.redirect(`/admin/integrantes/crear?mensaje="Todos los campos son obligatorios. Verifique los datos"`
    );
});

router.post('/Media/create', (req, res) => {
    if (!req.body.matricula || req.body.matricula.trim() === '')
        return res.redirect('/admin/Media/crear?mensaje="El campo matricula no puede estar vacio. Por favor Verifique"!');

});
router.post('/TipoMedia/create', (req, res) => {
    if (!req.body.nombre || req.body.nombre.trim() === '')
        return res.redirect('/admin/Media/crear?mensaje="El campo Nombre no puede estar vacio. Por favor Verifique"!');

});



module.exports = router; 


