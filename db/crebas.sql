-- Tabla para almacenar los integrantes
CREATE TABLE IF NOT EXISTS Integrantes (
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    matricula VARCHAR(20),
    nombre VARCHAR(45) NOT NULL,
    apellido VARCHAR(45) NOT NULL,  
    url VARCHAR(45),
    orden INTEGER
);
-- Tabla para almacenar los tipos de media
CREATE TABLE IF NOT EXISTS TipoMedia (
    id_tipo_media INTEGER PRIMARY KEY AUTOINCREMENT,
    nombre VARCHAR(20),
    orden INTEGER
);


-- Tabla para almacenar los medios
CREATE TABLE IF NOT EXISTS Media (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    titulo VARCHAR(45),
    nombre VARCHAR(45),
    url VARCHAR(45),
    matricula VARCHAR(20) NOT NULL,
    id_tipo_media INT NOT NULL,
    orden INTEGER,
    FOREIGN KEY (matricula) REFERENCES Integrantes(matricula),
    FOREIGN KEY (id_tipo_media) REFERENCES TipoMedia(id_tipo_media)
);

CREATE TABLE IF NOT EXISTS Inicio (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    titulo VARCHAR(45),
    url VARCHAR(45)
);
-- Índices
CREATE INDEX IF NOT EXISTS idx_tipo_media ON Media (id_tipo_media);
CREATE INDEX IF NOT EXISTS idx_media_integrante ON Media (matricula);
